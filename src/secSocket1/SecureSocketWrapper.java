package secSocket1;

import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.net.DatagramPacket;

import java.net.InetAddress;
import java.net.SocketException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

/**Esta classe serve para gerir as sockets de SecureDatagramSocket e MulticastDatagramSocket. Apenas atua como um wrapper e serve
 * para-se poder usar com endereços uni e multicast.
 * **/
public class SecureSocketWrapper {
    private SecureDatagramSocket uni_socket;
    private SecureMulticastSocket multicst_socket;
    private InetAddress addr;

    public SecureSocketWrapper(String host, int port) throws IOException, NoSuchPaddingException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, NoSuchProviderException, CertificateException {
        //Primeiro vemos se o endereço é unicast ou multicast
        addr = InetAddress.getByName(host);
        if(addr.isMulticastAddress())
            multicst_socket = new SecureMulticastSocket(host,port);

        else{
            if(port == 0)
                //Server side
                uni_socket = new SecureDatagramSocket(host);
            //Client side
            else uni_socket = new SecureDatagramSocket(host,port);
        }
    }

    public void send(DatagramPacket packet) throws IOException {
        if(addr.isMulticastAddress())
            multicst_socket.send(packet);
        else uni_socket.send(packet);

    }

    public void receive(DatagramPacket packet) throws IOException {
        if(addr.isMulticastAddress())
            multicst_socket.receive(packet);
        else uni_socket.receive(packet);
    }

    public void close(){
        if(addr.isMulticastAddress())
            multicst_socket.destroy();
        else uni_socket.destroy();
    }
    public void setSoTimeout(int timeout) throws SocketException {
        if(addr.isMulticastAddress())
            multicst_socket.setSoTimeout(timeout);
        else uni_socket.setSoTimeout(timeout);
    }
}
