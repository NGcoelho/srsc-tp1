package secSocket1;

import conf.CipherConf;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.*;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class SecureDatagramSocket extends DatagramSocket {

    private static final int HEADER_SIZE=6; //6bytes
    private Cipher cipher;
    private CipherConf conf;
    private List<String> nounceList= new ArrayList<>();


   SecureDatagramSocket(String address, SocketAddress bindaddr) throws IOException, NoSuchProviderException, NoSuchAlgorithmException, NoSuchPaddingException, UnrecoverableKeyException, KeyStoreException, CertificateException {
        super(bindaddr);
        conf = Encoder.getCipher(address);
        cipher = Cipher.getInstance(conf.getCiphersuite(), conf.getProvider());
        Encoder.setCipher_key();
        Encoder.setMacIKey();
        Encoder.setMacAKey();
    }

    SecureDatagramSocket(String address, int port) throws IOException, NoSuchProviderException, NoSuchAlgorithmException, NoSuchPaddingException, UnrecoverableKeyException, KeyStoreException, CertificateException {
        super(port);
        conf = Encoder.getCipher(address);
        cipher = Cipher.getInstance(conf.getCiphersuite(), conf.getProvider());
        Encoder.setCipher_key();
        Encoder.setMacIKey();
        Encoder.setMacAKey();

    }

    SecureDatagramSocket(String address, int port, InetAddress laddr) throws IOException, NoSuchProviderException, NoSuchAlgorithmException, NoSuchPaddingException, UnrecoverableKeyException, KeyStoreException, CertificateException {
        super(port, laddr);
        conf = Encoder.getCipher(address);
        cipher = Cipher.getInstance(conf.getCiphersuite(), conf.getProvider());
        Encoder.setCipher_key();
        Encoder.setMacIKey();
        Encoder.setMacAKey();
    }

    SecureDatagramSocket(String address) throws IOException, NoSuchProviderException, NoSuchAlgorithmException, NoSuchPaddingException, UnrecoverableKeyException, KeyStoreException, CertificateException {
        super();
        conf = Encoder.getCipher(address);
        cipher = Cipher.getInstance(conf.getCiphersuite(), conf.getProvider());
        Encoder.setCipher_key();
        Encoder.setMacIKey();
        Encoder.setMacAKey();
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void send(DatagramPacket packet){
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            byte[] encoded = Encoder.encode(cipher,conf,packet);
            out.write(Encoder.generateHeader((short)encoded.length));
            out.write(encoded);
            packet.setData(out.toByteArray());
            packet.setLength(out.size());
            super.send(packet);
            encoded=null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @SuppressWarnings("Duplicates")
    @Override
    public void receive(DatagramPacket packet) throws IOException {
        super.receive(packet);
        //byte[] header = Arrays.copyOfRange(packet.getData(),0,HEADER_SIZE);
        byte[] encoded = Arrays.copyOfRange(packet.getData(),HEADER_SIZE, packet.getLength());
        byte[] decoded = Encoder.decode(cipher,conf, nounceList,encoded);
        packet.setData(decoded);
    }

    public void destroy(){
       super.close();
       Encoder.destroy();
    }

}
