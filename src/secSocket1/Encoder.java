package secSocket1;

import conf.CipherConf;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;

import conf.CipherConfReader;
import exceptions.NoSuchAddressException;
import exceptions.NounceAlreadyExistsException;
import exceptions.TamperedMessageException;
import utils.Nonce;

 class Encoder {
    /**Esta classe contem todos os metodos necessários para proceder a encriptação e autenticação  das mensagens
     * e é usada pela classe SecureMulticastSocket e SecureDatagramSocket. Todos os métodos são estáticos.
     * **/
    private static final byte SEPARATOR=0X00;
    private static final int id=1;
    private static final int MAX_SIZE=1316;
    private static Key cipher_key;
    private static Key h_mac_in_key;
    private static Key h_mac_a_key;
    private static final String VERSION="0";
    private static final String RELEASE="1";
    private static final byte PAYLOAD_TYPE=0x01;
    private static final int HEADER_SIZE=6; //6bytes

    static void setCipher_key() throws UnrecoverableKeyException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
         cipher_key = getFromKeystore("mykeystore.jks",
                 "afe94c9b0202","JCEKS","aes2key","afe94c9b0202");
    }

    static void setMacIKey() throws UnrecoverableKeyException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        h_mac_in_key = getFromKeystore("mykeystore.jks","afe94c9b0202","JCEKS",
                "hmacinkey","afe94c9b0202");
    }
    static void setMacAKey() throws UnrecoverableKeyException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        h_mac_a_key =  getFromKeystore("mykeystore.jks",
                "afe94c9b0202","JCEKS","hmacankey","afe94c9b0202");
    }

     static byte[] encode(Cipher cipher, CipherConf conf, DatagramPacket packet) {
        //TODO
        //AVOID ASSIGNING TO VARIABLES, IF YOU DO, NULL THEM AFTERWARDS
        try {
            ByteArrayOutputStream message_payload = new ByteArrayOutputStream();
            //CONCATENATION OF MP: ID||0X00||NONCE||0X00||M
            message_payload.write(Integer.toString(id).getBytes());
            message_payload.write(SEPARATOR);
            message_payload.write(generateNounce());
            message_payload.write(SEPARATOR);
            message_payload.write(packet.getData());

            //CREATE HASH(MP)
            Mac h_mac = Mac.getInstance(conf.getMacKm(),conf.getProvider());
            h_mac.init(h_mac_in_key);
            h_mac.update(message_payload.toByteArray());
            //HASH(MP)||MP
            message_payload.write(h_mac.doFinal());

            //CIPHER THE PREVIOUS CONCATENATION
            IvParameterSpec ivParameters =  generateIVParameters();
            if(cipher.getAlgorithm().contains("ECB")||cipher.getAlgorithm().contains("RC4"))
                cipher.init(Cipher.ENCRYPT_MODE,cipher_key);
            else
                cipher.init(Cipher.ENCRYPT_MODE,cipher_key,ivParameters);
            byte[] encrypted = cipher.doFinal(message_payload.toByteArray());
            message_payload.close();
            h_mac = null;


            //CREATE HASH FOR DDOS MITGATION
            Mac h_mac_a = Mac.getInstance(conf.getMacKa(),conf.getProvider());
            h_mac_a.init(h_mac_a_key);
            h_mac_a.update(encrypted);

            //RETURN YOUR BEAUTIFUL CREATION
            // E (KS, [ Mp || MACKM (Mp) ] ) || MACKA (C)
            ByteArrayOutputStream encoded = new ByteArrayOutputStream();
            encoded.write(encrypted);
            encoded.write(h_mac_a.doFinal());
            h_mac_a=null;
            encrypted=null;
            return encoded.toByteArray();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

     static byte[] decode(Cipher cipher, CipherConf conf, List<String> nounceList, byte[] packet){
         //TODO
         try {
             //PRIMEIRO TEMOS DE VERIFICAR O HASH DDOS
             int pkt_len=packet.length;
             Mac h_mac_a = Mac.getInstance(conf.getMacKa(),conf.getProvider());
             //vamos buscar os bytes correspondentes ao h_mac_a na mensagem
             //COPIAR HASH PARA ESTE ARRAY
             byte[] h_mac_a_string = Arrays.copyOfRange(packet,(pkt_len-h_mac_a.getMacLength()),pkt_len);
             byte[] encrypted = Arrays.copyOfRange(packet,0,(pkt_len-h_mac_a.getMacLength()));


             h_mac_a.init(h_mac_a_key);
             h_mac_a.update(encrypted);
             //CHECKING IF HASHES MATCH, IF SO WE CAN CONTINUE


             byte [] newmac = h_mac_a.doFinal();
             if(!MessageDigest.isEqual(newmac,h_mac_a_string)) {
                 System.out.println("HMACASTRING: "+ h_mac_a_string);
                 throw new TamperedMessageException("Hashes don't match!!!");
             }


             //DECRYPTING [ Mp || MACKM (Mp) ]
             if(cipher.getAlgorithm().contains("ECB")||cipher.getAlgorithm().contains("RC4"))
                cipher.init(Cipher.DECRYPT_MODE,cipher_key);
             else cipher.init(Cipher.DECRYPT_MODE,cipher_key, generateIVParameters());
             byte[] decrypted = cipher.doFinal(encrypted);
             encrypted=null;
             h_mac_a = null;
             h_mac_a_string=null;

             //AGORA QUE TEMOS [ Mp || MACKM (Mp) ] temos de  ver o hash de MP
             Mac h_mac = Mac.getInstance(conf.getMacKm(),conf.getProvider());
             byte[] h_mac_string = new byte[h_mac.getMacLength()];
             System.arraycopy(decrypted,decrypted.length-h_mac.getMacLength(),h_mac_string,0,h_mac.getMacLength());
             h_mac.init(h_mac_in_key);
             h_mac.update(decrypted,0,decrypted.length-h_mac.getMacLength());
             if(!MessageDigest.isEqual(h_mac.doFinal(),h_mac_string))
                 throw new TamperedMessageException("Hashes don't match!!!");
             h_mac_string = null;



             //PASSOU TUDO!! agora temos de ir buscar M
             //porque MP: ID||0X00||NONCE||0X00||M
             byte[] payload_bytes = Arrays.copyOfRange(decrypted,0,decrypted.length-h_mac.getMacLength());
             h_mac = null;
             byte[] payload_data= new byte[MAX_SIZE];
             String nounce=null;
             int i_nounce=-1, separator_counter=0, i=0;
             while(i < payload_bytes.length){
                 if(payload_bytes[i]==SEPARATOR)
                     separator_counter++;
                 if(separator_counter == 1 && i_nounce == -1)
                     //AQUI COMEÇA O NOUNCE NA MENSAGEM(em i+1)
                     i_nounce = i+1;
                 if (separator_counter == 2) {
                     //AQUI ESTÁ A MENSAGEM( M )
                     nounce = new String(Arrays.copyOfRange(payload_bytes,i_nounce,i));
                     System.arraycopy(payload_bytes,i+1,payload_data,0,(payload_bytes.length-(i+1)));
                     break;
                 }
                 i++;
             }

             //VERIFICAMOS QUE NOUNCE É GENUINO
             if(nounceList.contains(nounce))
                 throw new NounceAlreadyExistsException("DUPLICATED NOUNCE! DANGER!");
             nounceList.add(nounce);
             payload_bytes=null;
             nounce=null;
             return payload_data;
         }catch (Exception e){
             e.printStackTrace();
             return null;
         }

     }

     static byte[] generateHeader(short pl_size) throws Exception {
         ByteArrayOutputStream out = new ByteArrayOutputStream();
         out.write(VERSION.getBytes());
         out.write(RELEASE.getBytes());
         out.write(SEPARATOR);
         out.write(PAYLOAD_TYPE);
         out.write(SEPARATOR);
         out.write(pl_size);
         if(out.toByteArray().length != HEADER_SIZE)
             throw new Exception("UPSIE DUPSIE");
         return out.toByteArray();
     }

     static CipherConf getCipher(String mcastAddress) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException {
        CipherConf conf = CipherConfReader.readConfFile().get(mcastAddress);
         if(conf == null)
             throw new NoSuchAddressException();
        return conf;
     }


     private static byte[] generateNounce(){
        return Nonce.generateNounce().getBytes();
    }

    private static IvParameterSpec generateIVParameters() {
        // byte[] random =  new byte[1316];
        //new Random().nextBytes(random);
        //devia-se usar secure random
        //reduzir o array p/ 8 bytes se o algoritmo o pedir
        byte[]  iv = new byte[] {
                0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
        };
        return new IvParameterSpec(iv);
    }

     private static Key getFromKeystore(String store, String store_pw, String type,
                                 String key_name, String key_pw)
             throws KeyStoreException, IOException, CertificateException,
             NoSuchAlgorithmException, UnrecoverableKeyException {
         KeyStore k_store = KeyStore.getInstance(type);
         k_store.load(new FileInputStream(store),store_pw.toCharArray());
         Key key = k_store.getKey(key_name,key_pw.toCharArray());
         //leave no traces behind to them sneaky bastards
         k_store=null;
         return key;
     }


     static void destroy() {
        cipher_key = null;
        h_mac_in_key = null;
        h_mac_a_key = null;
     }
 }
