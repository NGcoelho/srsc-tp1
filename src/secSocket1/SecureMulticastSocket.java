package secSocket1;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketAddress;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import conf.*;



class SecureMulticastSocket extends MulticastSocket {

    private static final int HEADER_SIZE=6; //6bytes
    private Cipher cipher;
    private CipherConf conf;
    private List<String> nounceList= new ArrayList<>();


    SecureMulticastSocket(String mcastAddress, int port) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, CertificateException, KeyStoreException, UnrecoverableKeyException {
        super(port);
        this.joinGroup(InetAddress.getByName(mcastAddress));
        conf = Encoder.getCipher(mcastAddress);
        cipher = Cipher.getInstance(conf.getCiphersuite(), conf.getProvider());
        Encoder.setCipher_key();
        Encoder.setMacIKey();
        Encoder.setMacAKey();
        //


    }
    SecureMulticastSocket(String mcastAddress, SocketAddress bindaddr) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, CertificateException, KeyStoreException, UnrecoverableKeyException {
        super(bindaddr);
        conf = Encoder.getCipher(mcastAddress);
        cipher = Cipher.getInstance(conf.getCiphersuite(), conf.getProvider());
        Encoder.setCipher_key();
        Encoder.setMacIKey();
        Encoder.setMacAKey();

    }
    @SuppressWarnings("Duplicates")
    @Override
    public void send(DatagramPacket packet) throws IOException{
        //TODO

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            byte[] encoded = Encoder.encode(cipher,conf,packet);
            out.write(Encoder.generateHeader((short)encoded.length));
            out.write(encoded);
            packet.setData(out.toByteArray());
            packet.setLength(out.size());
            super.send(packet);
            encoded=null;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @SuppressWarnings("Duplicates")
    @Override
    public void receive(DatagramPacket packet) throws IOException{
        //TODO
        super.receive(packet);
        //byte[] header = Arrays.copyOfRange(packet.getData(),0,HEADER_SIZE);
        byte[] encoded = Arrays.copyOfRange(packet.getData(),HEADER_SIZE, packet.getLength());
        byte[] decoded = Encoder.decode(cipher,conf, nounceList,encoded);
        packet.setData(decoded);



    }
    public void destroy(){
        super.close();
        Encoder.destroy();
    }


}
