package exceptions;

public class NoSuchAddressException extends RuntimeException{
    public NoSuchAddressException(){
        super();
    }
    public NoSuchAddressException(String message){
        super(message);
    }
}
