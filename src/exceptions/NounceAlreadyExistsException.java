package exceptions;

public class NounceAlreadyExistsException extends RuntimeException {
    public NounceAlreadyExistsException(){
        super();
    }
    public NounceAlreadyExistsException(String message){
        super(message);
    }
}

