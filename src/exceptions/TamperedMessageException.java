package exceptions;

public class TamperedMessageException extends Exception{
    public TamperedMessageException(){
        super();
    }
    public TamperedMessageException(String message){
        super(message);
    }
}
