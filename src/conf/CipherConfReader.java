package conf;

import java.io.*;
import java.util.HashMap;
import java.util.Properties;
import java.util.Scanner;

public class CipherConfReader {
    /**
     * Esta classe vai gerar um HashMap  de ficheiros de configuração(valor), para cada um dos IP's(chave)
     * do ficheiro ciphersuite.conf
     * **/


    private static final String conf_file_name = "./src/conf/ciphersuite.conf";
    private static HashMap<String,CipherConf> conf_by_ip = new HashMap<>();

    public static HashMap<String,CipherConf> readConfFile() throws IOException {

        CipherConf conf;
        Properties props = new Properties();
        File conf_file= new File(conf_file_name);
        Scanner sc = new Scanner(conf_file);
        while(sc.hasNextLine()) {
            String next = sc.nextLine();
            //Aqui começa nova config pa ip nesta linha
            if(next.contains("<") && !next.contains("</")){
                String ip = next.replace("<","").replace(">","").trim();
                StringBuilder sb = new StringBuilder();
                while(sc.hasNextLine() ){
                    next = sc.nextLine();
                    if(!next.contains("</"))
                       sb.append(next+"\n");
                    else break;
                }
                //FAZER AQUI CONFIGURACAO
                StringReader reader = new StringReader(sb.toString());
                props.load(reader);
                conf = prepareCipherConf(ip, props);
                reader.close();
                conf_by_ip.put(ip,conf);
            }
        }



        return conf_by_ip;
    }
        //POR AGORA ASSUMAMOS QUE O PROVIDER SERA SEMPRE O BOUNCY CASTLE, DEPOIS MUDA SE
    private static CipherConf prepareCipherConf(String ip, Properties properties) {

        return new CipherConf(ip,properties.getProperty("CIPHERSUITE"),
               "BC",
                properties.getProperty("MACKM"),
                properties.getProperty("MACKA"));
    }
}
