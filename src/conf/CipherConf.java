package conf;

public class CipherConf {
    /**
     * Esta classe vai albergar CADA UMA das configurações de cipher para cada um dos endereços Ip especificados no ficheiro
     * ciphersuite.conf (<224.10.10.10> bla bla bla </224.10.10.10> e por ai )
     * Ks: symmetric session key
     * KM: MAC key
     * KA: a MAC key for Fast control DoS mitigation
     **/
    private String ip_addr;
    private String ciphersuite, provider;
    private String macKm;
    private String macKa;

    public CipherConf(String ip_addr, String ciphersuite, String provider,
                      String macKm, String macKa) {
        this.ip_addr = ip_addr;
        this.ciphersuite = ciphersuite;
        this.provider = provider;
        this.macKm = macKm;
        this.macKa = macKa;

    }

    public String getIp_addr() {
        return ip_addr;
    }

    public void setIp_addr(String ip_addr) {
        this.ip_addr = ip_addr;
    }

    public String getCiphersuite() {
        return ciphersuite;
    }

    public void setCiphersuite(String ciphersuite) {
        this.ciphersuite = ciphersuite;
    }


    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }


    public String getMacKm() {
        return macKm;
    }

    public void setMacKm(String macKm) {
        this.macKm = macKm;
    }


    public String getMacKa() {
        return macKa;
    }

    public void setMacKa(String macKa) {
        this.macKa = macKa;
    }

}
