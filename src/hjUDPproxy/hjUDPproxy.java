package hjUDPproxy;/* hjUDPproxy, 20/Mar/18
 *
 * This is a very simple (transparent) UDP proxy
 * The proxy can listening on a remote source (server) UDP sender
 * and transparently forward received datagram packets in the
 * delivering endpoint
 *
 * Possible Remote listening endpoints:
 *    Unicast IP address and port: configurable in the file config.properties
 *    Multicast IP address and port: configurable in the code
 *  
 * Possible local listening endpoints:
 *    Unicast IP address and port
 *    Multicast IP address and port
 *       Both configurable in the file config.properties
 */

import secSocket1.SecureSocketWrapper;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.*;
import java.util.Arrays;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

public class hjUDPproxy {
    private static String host = "";
    private static int port = -1 ;
    private static int timeout = 20000;
    public static void main(String[] args) throws Exception {
        InputStream inputStream = new FileInputStream("config.properties");
        if (inputStream == null) {
            System.err.println("Configuration file not found!");
            System.exit(1);
        }
        Properties properties = new Properties();
        properties.load(inputStream);
	    String remote = properties.getProperty("remote");
        String destinations = properties.getProperty("localdelivery");

       // SocketAddress inSocketAddress =
        parseSocketAddress(remote,true);
        Set<SocketAddress> outSocketAddressSet = Arrays.stream(destinations.split(",")).map(s -> parseSocketAddress(s,false)).collect(Collectors.toSet());

        /* If listen a remote unicast server taje the remote config
         * uncomment the following line
	 */
        //DatagramSocket inSocket = new DatagramSocket(inSocketAddress);

        /* If listen a remote multicast server in 239.9.9.9 port 9999
	 * uncomment the following two lines
	 */
	   //SecureMulticastSocket ms = new SecureMulticastSocket(host,Integer.valueOf(port));
        //SecureDatagramSocket inSocket = new SecureDatagramSocket(host,port);
        SecureSocketWrapper inSocket = new SecureSocketWrapper(host,port);
        //MulticastSocket ms = new MulticastSocket(port);
        try{
            inSocket.setSoTimeout(timeout);
            System.out.println("Resolving address : " + InetAddress.getByName(host)+": "+port);
            //ms.joinGroup(InetAddress.getByName(host));


            DatagramSocket outSocket = new DatagramSocket();
            //byte[] buffer = new byte[1397];
            byte[] buffer = new byte[2048];

            while (true) {
                DatagramPacket inPacket = new DatagramPacket(buffer, buffer.length);
                /* If listen a remote unicast server
                 * uncomment the following line
                 */
                inSocket.receive(inPacket);  // if remote is unicast

                /* If listen a remote multcast server
                 * uncomment the following line
                 *
                 */
                //ms.receive(inPacket);          // if remote is multicast


                System.out.print("*");
                for (SocketAddress outSocketAddress : outSocketAddressSet)
                {      // System.out.println("SENDING TO ADDRESS: "+ outSocketAddress.toString());
                    outSocket.send(new DatagramPacket(inPacket.getData(), inPacket.getLength(), outSocketAddress));
                }
            }
        }catch (SocketTimeoutException e){
            System.out.println("Sender never sent or has stopped sending packets.\nBye now :-)");
            inSocket.close();
        }
    }

    private static InetSocketAddress parseSocketAddress(String socketAddress, boolean remote) {
        String[] split = socketAddress.split(":");
        if (remote){
            host = split[0];
            port = Integer.parseInt(split[1]);
        }
        return new InetSocketAddress(split[0],Integer.parseInt(split[1]));
    }
}
