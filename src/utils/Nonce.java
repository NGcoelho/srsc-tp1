package utils;

import java.security.SecureRandom;
import java.util.Random;

public class Nonce {
    //String a ser usada para construir os nounces
    private final static String S_NONCE="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789-/?_.,:;(){}[]";
    private final static int nonce_length=20;

    private static Random sec_rand=new SecureRandom();

    //PARA A SEGUNDA FASE BOTA-SE BOOLEAN NO PARAMETRO PARA SABER SE É TLS OU SAP
    public static String generateNounce(){
        StringBuilder builder = new StringBuilder(nonce_length);
        for (int i = 0; i < nonce_length; i++) {
            //O sec_rand é bounded pelo tamanho de S_NOUNCE, e irá escolher um dos caracteres(posicao) para concatenar
            builder.append(S_NONCE.charAt(sec_rand.nextInt(S_NONCE.length())));
        }
        return builder.toString();
    }
}
